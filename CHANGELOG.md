# CHANGELOG

<!--- next entry here -->

## 0.1.1
2020-06-16

### Fixes

- change method protection (043e6fe8e016164b06178b2a299d130ac584396a)

## 0.1.0
2020-06-16

### Features

- initial commit (90ac0b141d297ac4420e57bd6b382c74af954613)