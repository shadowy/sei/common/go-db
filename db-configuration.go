package db

import "gitlab.com/shadowy/go/postgres"

type Configuration interface {
	GetDB() *postgres.Database
}
