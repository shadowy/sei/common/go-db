module gitlab.com/shadowy/sei/common/go-db

go 1.14

require (
	github.com/sirupsen/logrus v1.6.0 // indirect
	gitlab.com/shadowy/go/application-settings v1.0.12 // indirect
	gitlab.com/shadowy/go/postgres v1.0.12 // indirect
)
